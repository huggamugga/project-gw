from django.test import TestCase, Client
from django.urls import resolve
from .views import index, friend_list, friend_list_json, add_friend, delete_friend, validate_npm
from .models import Friend
from .api_csui_helper.csui_helper import CSUIhelper

# Create your tests here.
class Lab7UnitTest(TestCase):

	def test_lab_7_url_is_exist(self):
		response = Client().get('/lab-7/')
		self.assertEqual(response.status_code, 200)

	def test_lab_7_using_index_func(self):
		found = resolve('/lab-7/')
		self.assertEqual(found.func, index)

	def test_model_can_create_Friends(self):
		# Creating a new activity
		new_activity = Friend.objects.create(friend_name='bambang', npm='00000000')

		# Retrieving all available activity
		counting_all_available_Friends = Friend.objects.all().count()
		self.assertEqual(counting_all_available_Friends, 1)

	def test_views_as_dict(self):
		response = Client().get('/lab-7/get-friend-list/')
		self.assertEqual(response.status_code, 200)
   
	def test_get_friend_list_url_using_friend_list_func(self):
		found = resolve('/lab-7/get-friend-list/')
		self.assertEqual(found.func, friend_list_json)


	def test_page_so_much(self):
		response = Client().get('/lab-7/?page=2000')
		self.assertEqual(response.status_code,200)

	def test_Daftar_teman(self):
		response = Client().get('/lab-7/daftar-teman/')
		self.assertEqual(response.status_code,200)

	def test_model_can_delete_friend(self):
		new_friend = Friend.objects.create(friend_name='Kak Pewe', npm='1406917922')
		counting_all_available_friend = Friend.objects.all().count()

		response= Client().get('/lab-7/get-friend-list/')
		html_response = response.content.decode('utf8')

		delete_friend(html_response,Friend.objects.get(friend_name='Kak Pewe').npm)

		counting_after_delete = Friend.objects.all().count()

		self.assertEqual(counting_all_available_friend-1,counting_after_delete)

	def test_validate_npm(self):
		new_friend = Friend.objects.create(friend_name='Kak Pewe', npm='1406917922')
		response = Client().post('/lab-7/validate-npm/', {'npm': new_friend.npm})
		self.assertEqual(dict, type(response.json()))

	def test_lab7_add_friend_success(self):
		name = 'Kak Pewe'
		npm = '1406917922'
		data = {'name': name, 'npm': npm}
		response_post = Client().post('/lab-7/add-friend/', data)
		self.assertEqual(response_post.status_code, 200)

	def test_lab7_can_collect_all_data_from_API(self):
		csui_helper = CSUIhelper()
		mahasiswa_list = csui_helper.instance.get_mahasiswa_list()
		self.assertNotEqual(len(mahasiswa_list), 0)
