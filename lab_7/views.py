from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from .models import Friend
from .api_csui_helper.csui_helper import CSUIhelper
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core import serializers
import os

response = {}
csui_helper = CSUIhelper()

def index(request):
    mahasiswa_list = csui_helper.instance.get_mahasiswa_list()
    friend_list = Friend.objects.all()
    paginator = Paginator(mahasiswa_list, 20)
    page = request.GET.get('page')
    try:
        teman = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        teman = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        teman = paginator.page(paginator.num_pages)

    response['mahasiswa_list'] = teman
    response['friend_list'] = friend_list
    html = 'lab_7/lab_7.html'
    return render(request, html, response)

def friend_list(request):
    friend_list = Friend.objects.all()
    response['friend_list'] = friend_list
    html = 'lab_7/daftar_teman.html'
    return render(request, html, response)

def friend_list_json(request): # update
    friends = [obj.as_dict() for obj in Friend.objects.all()]
    return JsonResponse({"results": friends}, content_type='application/json')

def coba_serialize(request):
    if request.method == 'GET':
        friend_list = Friend.objects.all()
        data = serializers.serialize("json", friend_list)
        return HttpResponse(data)

@csrf_exempt
def add_friend(request):
    if request.method == 'POST':
        npm = request.POST['npm']
        name = request.POST['name']
        friend = Friend(friend_name=name, npm=npm)
        friend.save()
        return JsonResponse(friend.as_dict())

def delete_friend(request, friend_id):
    Friend.objects.filter(npm=friend_id).delete()
    return HttpResponseRedirect('/lab-7/')

@csrf_exempt
def validate_npm(request):
    npm = request.POST.get('npm', None)
    data = {
        'is_taken': Friend.objects.filter(npm=npm).exists()
    }
    return JsonResponse(data)

def listing(request):
    contact_list = Contacts.objects.all()
    paginator = Paginator(contact_list, 25) # Show 25 contacts per page

    page = request.GET.get('page')
    try:
        contacts = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        contacts = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        contacts = paginator.page(paginator.num_pages)

    return render(request, 'list.html', {'contacts': contacts})
