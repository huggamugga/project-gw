## Checklist

1.  Membuat Halaman Chat Box
    1. [x] Tambahkan _lab_6.html_ pada folder templates
	sudah ditambahkan dan dapat diakses di [TEMPLATE HTML](https://gitlab.com/huggamugga/project-gw/blob/master/lab_6/templates/lab_6/lab_6.html)
    2. [x] Tambahkan _lab_6.css_ pada folder _./static/css_
	sudah ditambahkan dan dapat diakses di [FILE CSS](https://gitlab.com/huggamugga/project-gw/blob/master/lab_6/static/css/lab_6.css)
    3. [x] Tambahkan file _lab_6.js_ pada folder _lab_6/static/js_
	sudah ditambahkan dan dapat diakses di [FILE JS](https://gitlab.com/huggamugga/project-gw/blob/master/lab_6/static/js/lab_6.js)
    4. [x] Lengkapi potongan kode pada _lab_6.js_ agar dapat berjalan
	sudah dilengkapi

2. Mengimplementasikan Calculator
    1. [x] Tambahkan potongan kode ke dalam file _lab_6.html_ pada folder templates
	sudah ditambahkan dan dapat diakses di [TEMPLATE HTML](https://gitlab.com/huggamugga/project-gw/blob/master/lab_6/templates/lab_6/lab_6.html) di line 32 - 83
    2. [x] Tambahkan potongan kode ke dalam file _lab_6.css_ pada folder _./static/css_
	sudah ditambahkan dan dapat diakses di [FILE CSS](https://gitlab.com/huggamugga/project-gw/blob/master/lab_6/static/css/lab_6.css)
    3. [x] Tambahkan potongan kode ke dalam file _lab_6.js_ pada folder _lab_6/static/js_
	sudah ditambahkan dan dapat diakses di [FILE JS](https://gitlab.com/huggamugga/project-gw/blob/master/lab_6/static/js/lab_6.js)
    4. [x] Implementasi fungsi `AC`.
	sudah ditambahkan dan dapat diakses di [FILE JS](https://gitlab.com/huggamugga/project-gw/blob/master/lab_6/static/js/lab_6.js) di line 15 - 16

3.  Mengimplementasikan select2
    1. [x] Load theme default sesuai selectedTheme
	sudah ditambahkan dan dapat diakses di [FILE JS](https://gitlab.com/huggamugga/project-gw/blob/master/lab_6/static/js/lab_6.js) di line 49 dan 60
    2. [x] Populate data themes dari local storage ke select2
	sudah ditambahkan dan dapat diakses di [FILE JS](https://gitlab.com/huggamugga/project-gw/blob/master/lab_6/static/js/lab_6.js) di line 61 - 63
    3. [x] Local storage berisi themes dan selectedTheme
	sudah ditambahkan dan dapat diakses di [FILE JS](https://gitlab.com/huggamugga/project-gw/blob/master/lab_6/static/js/lab_6.js) di line 51 - 55
    4. [x] Warna berubah ketika theme dipilih
	sudah ditambahkan dan dapat diakses di [FILE JS](https://gitlab.com/huggamugga/project-gw/blob/master/lab_6/static/js/lab_6.js) di line 64 - 67

4.  Pastikan kalian memiliki _Code Coverage_ yang baik
    1. [x]  Jika kalian belum melakukan konfigurasi untuk menampilkan _Code Coverage_ di Gitlab maka lihat langkah `Show Code Coverage in Gitlab` di [README.md](https://gitlab.com/PPW-2017/ppw-lab/blob/master/README.md).
    2. [x] Pastikan _Code Coverage_ kalian 100%.

###  Challenge Checklist
1. Latihan Qunit
    1. [ ] Implementasi dari latihan Qunit
1. Cukup kerjakan salah satu nya saja:
    1. Implementasikan tombol enter pada chat box yang sudah tersedia
        1. [ ] Buatlah sebuah _Unit Test_ menggunakan Qunit
        2. [ ] Bulah fungsi yang membuat _Unit Test_ Tersebut _passed_ 
    1. Implementasikan fungsi `sin`, `log`, dan `tan`. (HTML sudah tersedia di dalam potongan kode)
        1. [ ] Buatlah sebuah _Unit Test_ menggunakan Qunit
        2. [ ] Bulah fungsi yang membuat _Unit Test_ Tersebut _passed_